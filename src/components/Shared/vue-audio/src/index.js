import Vue from "vue";
import VueAudio from "./VueAudio.vue";

const Audio = new Vue({
  el: "#audio",
  components: {
    VueAudio
  }
});
