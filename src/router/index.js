import Vue from "vue";
import Router from "vue-router";
import Page from "@/components/Pages/Page";

import Post from "@/components/Posts/Post";
import Event from "@/components/Events/Event";
import Events from "@/components/Events/Calendar";
import Archive from "@/components/Posts/Archive";
import Category from "@/components/Posts/Category";
import Episode from "@/components/Podcasts/Episode";
import PodcastArchive from "@/components/Podcasts/Archive";
import SeriesArchive from "@/components/Podcasts/Series";
import SeriesList from "@/components/Podcasts/SeriesList";
import SpeakerArchive from "@/components/Podcasts/Speaker";
import Search from "@/components/Search/SearchResults";
import Media from "@/components/Media/Media";
import User from "@/components/User/User";
import Login from "@/components/User/Login";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Front",
      component: Page
    },
    {
      path: "/front",
      redirect: "/"
    },
    {
      path: "/blog",
      redirect: "/blog/page/1"
    },
    {
      path: "/blog/page/",
      redirect: "/blog/page/1"
    },
    {
      path: "/blog/page/:id",
      name: "Blog",
      component: Archive
    },
    {
      path: "/category/:id",
      redirect: "/category/:id/page/:page"
    },
    {
      path: "/category/:id/page/:page",
      name: "Category",
      component: Category
    },
    {
      path: "/podcast",
      redirect: "/podcast/page/1"
    },
    {
      path: "/podcast/page/",
      redirect: "/podcast/page/1"
    },
    {
      path: "/podcast/page/:id",
      name: "Podcasts",
      component: PodcastArchive
    },
    {
      path: "/series",
      name: "SeriesList",
      component: SeriesList
    },
    {
      path: "/series/:id",
      name: "Series",
      component: SeriesArchive
    },
    {
      path: "/speaker/:id",
      name: "Speaker",
      component: SpeakerArchive
    },
    {
      path: "/post/:id",
      name: "Post",
      component: Post
    },
    {
      path: "/media/:id",
      name: "Media",
      component: Media
    },
    {
      path: "/podcast/:id",
      name: "Podcast",
      component: Episode
    },

    {
      path: "/events",
      redirect: "/events/page/1"
    },
    {
      path: "/events/page/",
      redirect: "/events/page/1"
    },
    {
      path: "/events/page/:id",
      name: "Events",
      component: Events
    },
    {
      path: "/event/:id",
      name: "Event",
      component: Event
    },
    {
      path: "/search/:term",
      name: "Search",
      component: Search
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/user",
      name: "User",
      component: User
    },
    {
      path: "/:id",
      name: "Page",
      component: Page
    },
    {
      path: '*',
      redirect: "/not-found"
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ x: 0, y: 0 });
      }, 500);
    });
  }
});
