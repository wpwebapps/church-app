// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";

import AppHeader from "./components/Shared/Header";
import AppFooter from "./components/Shared/Footer";
import AppComments from "./components/Shared/Comments";
import AppSharing from "./components/Shared/Sharing";
import AppPlayer from "./components/Shared/Player";
import PostItem from "./components/Posts/PostItem";
import PodcastItem from "./components/Podcasts/PodcastItem";
import MailChimp from "./components/Shared/MailChimp";
import NotificationBar from "./components/Shared/NotificationBar";
import SearchForm from "./components/Shared/SearchForm";
import Pagination from "./components/Shared/Pagination";
import AppForm from "./components/Shared/Form";
import VueAudio from "./components/shared/vue-audio";
import AppSlider from "./components/Shared/Slider";
import store from "./store/store";
import Column from "./components/Pages/column";
import Row from "./components/Pages/row";

import VueTinySlider from 'vue-tiny-slider';
import VueFormGenerator from "vue-form-generator/dist/vfg-core.js";
import VueScrollReveal from "vue-scroll-reveal";
import SocialSharing from "vue-social-sharing/dist/vue-social-sharing.min.js"

require("./assets/main.scss");

Vue.config.productionTip = false;

Vue.use(SocialSharing);
Vue.use(VueScrollReveal);

Vue.component("app-header", AppHeader);
Vue.component("app-footer", AppFooter);
Vue.component("comments", AppComments);
Vue.component("sharing", AppSharing);
Vue.component("app-player", AppPlayer);
Vue.component("post-item", PostItem);
Vue.component("podcast-item", PodcastItem);
Vue.component("mailchimp", MailChimp);
Vue.component("notification-bar", NotificationBar);
Vue.component("search-form", SearchForm);
Vue.component("vue-audio", VueAudio);
Vue.component("pagination", Pagination);
Vue.component("app-form", AppForm);
Vue.component("vue-form-generator", VueFormGenerator);
Vue.component("tiny-slider", VueTinySlider);
Vue.component("app-slider", AppSlider);
Vue.component("column", Column);
Vue.component("row", Row);

/* eslint-disable no-new */
new Vue({
  el: "#app",
  store,
  router,
  template: "<App/>",
  components: {
    App,
    AppHeader,
    AppFooter,
    AppComments,
    AppSharing,
    PostItem,
    MailChimp,
    NotificationBar,
    SearchForm,
    VueAudio,
    Pagination,
    AppForm,
    VueFormGenerator,
    AppSlider,
    Column,
    Row
  }
});

ga('set', 'page', router.currentRoute.path);
ga('send', 'pageview');

router.afterEach((to, from) => {
  ga('set', 'page', to.path);
  ga('send', 'pageview');
});