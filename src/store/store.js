import Vue from "vue";
import Vuex from "vuex";
import { HTTP } from "./../environment";

Vue.use(Vuex);
const state = {
  loadedImage: false,
  podcastFile: "",
  options: {
    template_url: "",
    site_title: "",
    branding: {
      header_logo: "",
      footer_logo: ""
    },
    footer: {
      footer_text: ""
    },
    mailchimp: {
      include: false,
      intro_title: "",
      intro_subtitle: "",
      button_text: ""
    },
    header: {
      display_bar: false,
      intro_title: "",
      intro_subtitle: "",
      button_text: "",
      placeholder: "",
      confirmation: ""
    },
    transition: {
      type: ""
    },
    menu: []
  },
  user: {
    token: "",
    user_email: "",
    user_nicename: "",
    user_display_name: ""
  }
};
const mutations = {
  imageReady(state) {
    state.loadedImage = true;
  },
  imageNotReady(state) {
    state.loadedImage = false;
  },
  setPlayerMP3(state, mp3) {
    state.podcastFile = mp3;
  },
  setOptions: (state, options) => {
    state.options = options;
  },
  setUser: (state, user) => {
    state.user = user;
  }
};
const actions = {
  imageReady: ({ commit }) => commit("imageReady"),
  imageNotReady: ({ commit }) => commit("imageNotReady"),
  setPlayerMP3: ({ commit }) => commit("setPlayerMP3"),
  getOptions: ({ commit }) => commit("getOptions"),
  getUser: ({ commit }) => commit("getUser")
};
export default new Vuex.Store({
  state,
  actions,
  mutations
});
